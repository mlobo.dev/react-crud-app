import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from '../views/Home/Home';
import Users from '../views/users/Users';


// PAGES

function Router() {
  return (
    <BrowserRouter>
      <Route exact path="/" component={Home} />
      <Route exact path="/users/add" component={Users}/>
    </BrowserRouter>
  );
}
export default Router;
