import React, { useState, useRef } from "react";

import Container from "../../components/Container/Container";
import Navbar from "../../components/Navbar/Navbar";
import Panel from "../../components/Panel/Panel";
import { InputText } from "primereact/components/inputtext/InputText";
import { Button } from "primereact/button";
import "./Users.css";

import * as userService from "../../services/UserService";
import * as messages from "../../components/Toast/Toast";
import * as addressService from "../../services/AddressService";
import { Toast } from "primereact/toast";
export default function Users() {
  const toast = useRef(null);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [cep, setCep] = useState("");
  const [city, setCity] = useState("");
  const [uf, setUf] = useState("");
  const [gender, setGender] = useState("");

  function save() {
    userService
      .save({ name, email })
      .then(() => {
        messages.showSuccess(toast, "User created with Success");
      })
      .catch((erro) => {
        messages.showError(toast, erro.response.data.message);
      });
  }

  function handleChangeCEP(searchCep) {
    setCep(searchCep);

    if (searchCep.length >= 8) findAndUpdateAddressFieldsByCep(searchCep);
  }

  function findAndUpdateAddressFieldsByCep(searchCep) {
    addressService.findByCep(searchCep).then((response) => {
      debugger;
      setUf(response.data.uf);
      setCity(response.data.localidade);
    });
  }

  return (
    <>
      <Navbar />
      <Toast ref={toast} />
      <Container>
        <Panel>
          <div className="form-content">
            <div className="p-fluid p-formgrid p-grid">
              <div className="p-float-label p-col-12 p-md-6 p-lg-6  form-item">
                <InputText
                  id="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                <label htmlFor="name">Name</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-6 form-item">
                <InputText
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-2 form-item">
                <InputText
                  id="cep"
                  value={cep}
                  maxLength={8}
                  onChange={(e) => handleChangeCEP(e.target.value)}
                />
                <label htmlFor="cep">Cep</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-4 form-item">
                <InputText
                  id="city"
                  value={city}
                  onChange={(e) => setCity(e.target.value)}
                />
                <label htmlFor="city">City</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-1 form-item">
                <InputText
                  id="uf"
                  value={uf}
                  maxLength={2}
                  onChange={(e) => setUf(e.target.value)}
                />
                <label htmlFor="uf">UF</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-3 form-item">
                <InputText
                  id="phone"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
                <label htmlFor="uf">Phone</label>
              </div>

              <div className="p-float-label p-col-12 p-md-6 p-lg-2 form-item">
                <InputText
                  id="gender"
                  value={gender}
                  onChange={(e) => setGender(e.target.value)}
                />
                <label htmlFor="gender">Gender</label>
              </div>

              <span className="p-float-label p-col-12 p-md-6 p-lg-12">
                <Button label="Save" onClick={save} />
              </span>
            </div>
          </div>
        </Panel>
      </Container>
    </>
  );
}
