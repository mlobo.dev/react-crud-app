import React from 'react';
import { Menubar } from 'primereact/menubar';
import { Button } from 'primereact/button';

export default function Navbar() {

    const items = [
        {

            label: 'Users',
            icon: 'pi pi-fw pi-user',
            items: [
                {
                    label: 'New',
                    icon: 'pi pi-fw pi-user-plus',
                    url: '/users/add'



                },

                {
                    label: 'Search',
                    icon: 'pi pi-fw pi-users',
                    items: [
                        {
                            label: 'Filter',
                            icon: 'pi pi-fw pi-filter',
                            items: [
                                {
                                    label: 'Print',
                                    icon: 'pi pi-fw pi-print'
                                }
                            ]
                        },
                        {
                            icon: 'pi pi-fw pi-bars',
                            label: 'List'
                        }
                    ]
                }
            ]
        },

    ];
    return (
        <>
            <Menubar
                model={items}

                end={<Button label="Logout" icon="pi pi-power-off" />}
            />
        </>
    )

}