import React from "react";
import { Panel as PrimePainel } from "primereact/panel";

export default function Panel({ children }) {
  return (
    <>
      <PrimePainel header="Creating User" toggleable co>
        {children}
      </PrimePainel>
    </>
  );
}
