import React from "react";
import "./container.css";
export default function Container({ children }) {
  return (
    <>
      <div className="main-container">
        <div className="main-content">{children}</div>
      </div>
    </>
  );
}
