import "./ToastDemo.css";

export const showSuccess = (toast, message) => {
  toast.current.show({
    severity: "success",
    summary: "Created",
    detail: message,
    life: 3000,
  });
};

export const showInfo = (toast, message) => {
  toast.current.show({
    severity: "info",
    summary: "Info Message",
    detail: message,
    life: 3000,
  });
};

export const showWarn = (toast, message) => {
  toast.current.show({
    severity: "warn",
    summary: "Warn Message",
    detail: message,
    life: 3000,
  });
};

export const showError = (toast, message) => {
  toast.current.show({
    severity: "error",
    summary: "Error Message",
    detail: message,
    life: 3000,
  });
};
