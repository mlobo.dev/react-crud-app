import api from "../configuration/api";

const BASE_URI = "/users";

export async function save(user) {
  return api.post(BASE_URI, user);
}
