import api from "../configuration/api";

const BASE_URI = "/address";

export async function findByCep(cep) {
  return api.get(`${BASE_URI}/${cep}`);
}
